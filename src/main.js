import Vue from 'vue'
import App from './App.vue'
import Router from './routes.js'
import VueResource from 'vue-resource'
import Auth from './packages/auth/Auth.js'
import BootstrapVue from 'bootstrap-vue'
import VueBootstrapDatetimepicker from 'vue-bootstrap-datetimepicker'


Vue.use(VueResource);
Vue.use(BootstrapVue);
Vue.use(Auth);
Vue.use(VueBootstrapDatetimepicker);

Vue.http.options.root = "http://localhost/todoList/public";
Vue.http.headers.common['Authorization'] = 'Bearer '  + Vue.auth.getToken();

export const EventBus = new Vue();

Router.beforeEach(
    (to, from, next) => {
      if (to.matched.some(record => record.meta.forVisitors)) {
        if (Vue.auth.isAuthenticated()) {
          next({
              path: '/home'
          })
        } else next()
      } else if (to.matched.some(record => record.meta.forAuth)) {
          if (!Vue.auth.isAuthenticated()) {
              next({
                  path: '/login'
              })
          } else next()
      } else next()
    }
);

new Vue({
  el: '#app',
  render: h => h(App),
  router: Router
})
