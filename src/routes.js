import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Login        from  './components/auth/Login'
import Registration from  './components/auth/Registration'
import Home         from  './components/Home'

const router = new VueRouter({
    routes: [
        {
            path: "/",
            component: Login,
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/login",
            component: Login,
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/register",
            component: Registration,
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/home",
            component: Home,
            meta: {
                forAuth: true
            }
        }
    ]
});

export default router